// circular Sync

#include <mpi.h>
#include <iostream>
#include <random>

using namespace std;

int main(int argc, char** argv) {
	MPI_Init(NULL, NULL);
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);

	random_device rd;
	mt19937 e2(rd());
	uniform_real_distribution<> dist(0, 1000);
	int myValue = dist(e2), recvValue = 0;
	int largest = 0, smallest = 1001;

	//printf("%s:%d:%d\t myValue is %d\n", processor_name, world_size, world_rank, myValue);

	if(world_rank==0) {
		// MPI_Send(
		// 	void* data,
		// 	int count,
		// 	MPI_Datatype datatype,
		// 	int destination,
		// 	int tag,
		// 	MPI_Comm communicator);

		MPI_Send(&myValue, 1, MPI_INT, (world_rank + 1) % world_size, 0, MPI_COMM_WORLD); // smallest
		MPI_Send(&myValue, 1, MPI_INT, (world_rank + 1) % world_size, 1, MPI_COMM_WORLD); // largest

		// MPI_Recv(
		// 	void* data,
		// 	int count,
		// 	MPI_Datatype datatype,
		// 	int source,
		// 	int tag,
		// 	MPI_Comm communicator,
		// 	MPI_Status* status);

		MPI_Recv(&smallest, 1, MPI_INT, world_size - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(&largest, 1, MPI_INT, world_size - 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		//printf("%s:%d:%d\t smallest is %d\n", processor_name, world_size, world_rank, smallest);
		//printf("%s:%d:%d\t largest is %d\n", processor_name, world_size, world_rank, largest);

		// final round of sending
		MPI_Send(&smallest, 1, MPI_INT, (world_rank + 1) % world_size, 0, MPI_COMM_WORLD); // smallest
		MPI_Send(&largest, 1, MPI_INT, (world_rank + 1) % world_size, 1, MPI_COMM_WORLD); // largest
	}
	else {
		MPI_Recv(&recvValue, 1, MPI_INT, world_rank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		smallest = min(myValue, recvValue);
		MPI_Recv(&recvValue, 1, MPI_INT, world_rank - 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		largest = max(myValue, recvValue);
		//printf("%s:%d:%d\t smallest is %d\n", processor_name, world_size, world_rank, smallest);
		//printf("%s:%d:%d\t largest is %d\n", processor_name, world_size, world_rank, largest);
		MPI_Send(&smallest, 1, MPI_INT, (world_rank + 1) % world_size, 0, MPI_COMM_WORLD);
		MPI_Send(&largest, 1, MPI_INT, (world_rank + 1) % world_size, 1, MPI_COMM_WORLD);


		// final receive, then send.
		MPI_Recv(&smallest, 1, MPI_INT, world_rank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(&largest, 1, MPI_INT, world_rank - 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		if(world_rank < world_size-1) {
			MPI_Send(&smallest, 1, MPI_INT, (world_rank + 1) % world_size, 0, MPI_COMM_WORLD); // smallest
			MPI_Send(&largest, 1, MPI_INT, (world_rank + 1) % world_size, 1, MPI_COMM_WORLD); // largest
		}

		//printf("%s:%d:%d\t smallest is %d\n", processor_name, world_size, world_rank, smallest);
		//printf("%s:%d:%d\t largest is %d\n", processor_name, world_size, world_rank, largest);
	}

	MPI_Finalize();
}
