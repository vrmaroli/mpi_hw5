// centralized Solution

#include <mpi.h>
#include <iostream>
#include <random>

using namespace std;

int main(int argc, char** argv) {
	MPI_Init(NULL, NULL);
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);

	random_device rd;
	mt19937 e2(rd());
	uniform_real_distribution<> dist(0, 1000);
	int myValue = dist(e2), recvValue = 0;
	int largest = myValue, smallest = myValue;

	//printf("%s:%d:%d\t myValue is %d\n", processor_name, world_size, world_rank, myValue);

	// MPI_Send(
	// 	void* data,
	// 	int count,
	// 	MPI_Datatype datatype,
	// 	int destination,
	// 	int tag,
	// 	MPI_Comm communicator);

	// MPI_Recv(
	// 	void* data,
	// 	int count,
	// 	MPI_Datatype datatype,
	// 	int source,
	// 	int tag,
	// 	MPI_Comm communicator,
	// 	MPI_Status* status);

	for(int i = 0; i < world_size; i++) {
		if(i!=world_rank) {
			MPI_Send(&myValue, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
			MPI_Recv(&recvValue, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			smallest = min(smallest, recvValue);
			largest = max(largest, recvValue);
		}
	}
	
	//printf("%s:%d:%d\t smallest is %d\n", processor_name, world_size, world_rank, smallest);
	//printf("%s:%d:%d\t largest is %d\n", processor_name, world_size, world_rank, largest);


	MPI_Finalize();
}
